# Installation Requirements
Based on a lubuntu installation 2020-02-15 (Version 18.04.4 LTS X86-64)

## Desktop environment
 - Openbox   // Optional
 - AwesomeWm // Optional
 - I3        
 - Sway      // Optional
### Addons
 - i3wm-themer
 - polybar

## Terminals
 - X-Terminal-Emulator
 - URxvt     // optional

## Texteditors
 - Vim-gtk // copy & paste capabilities

### Addons
 - Vundle

## Browser
 - Firefox

## Filemanager
 - PCManFM
 - Dolphin  // Optional

## Docker
 - Plank

## Audio
 - Pulseaudio
 - Alsamixer

