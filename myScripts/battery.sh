#bin/sh

battery_print() {
PATH_AC="/sys/class/power_supply/AC"
PATH_BATTERY_0="/sys/class/power_supply/BAT0"
    PATH_BATTERY_1="/sys/class/power_supply/BAT1"

ac=0
battery_level_0=0
battery_level_1=0
battery_max_0=0
battery_max_1=0

    if [ -f "$PATH_AC/online" ]; then
            ac=$(cat "$PATH_AC/online")
    fi

    if [ -f "$PATH_BATTERY_0/energy_now" ]; then
            battery_level_0=$(cat "$PATH_BATTERY_0/energy_now")
fi

    if [ -f "$PATH_BATTERY_0/energy_full" ]; then
            battery_max_0=$(cat "$PATH_BATTERY_0/energy_full")
    fi

    if [ -f "$PATH_BATTERY_1/energy_now" ]; then
            battery_level_1=$(cat "$PATH_BATTERY_0/energy_now")
    fi

    if [ -f "$PATH_BATTERY_1/energy_full" ]; then
            battery_max_1=$(cat "$PATH_BATTERY_0/energy_full")
    fi


    battery_total_level=$(($battery_level_0+$battery_level_1))
    battery_total_max=$(($battery_max_0+$battery_max_1))
    battery_percent=$(echo "scale=2; $battery_total_level/$battery_total_max" | bc)
    battery_status_convert=$(echo "$battery_percent*100"|bc)
    battery_status=${battery_status_convert%.*}

    if [ "$ac" -eq 1 ]; then
        icon="AC"

        if [ $battery_status -gt 97 ]; then
            echo "$icon FULL"
        else
            echo "$icon $battery_status %"
        fi
    else
        icon="DC"
        crit=""
        if [ $battery_status -gt 38 ]; then
                crit=""
        elif [ $battery_status -gt 35 ]; then
                crit="!"
        elif [ $battery_status -gt 15 ]; then
                crit="!!"
        else
                crit="!!!"
        fi

        echo "$icon $battery_status % $crit"

    fi
}

path_pid="/tmp/polybar-battery-simplistic"

case "$1" in
    --update)
        pid=$(cat $path_pid)

        if [ "$pid" != "" ]; then
            kill -10 "$pid"
        fi
        ;;
        *)
            echo $$ > $path_pid
            trap exit INT
            trap "echo" USR1

            while true; do
                battery_print

                sleep 5 &
                wait
             done
             ;;
esac

