#/bin/sh
brightness_path=$(ls | grep -w brightness.tmp)
brightness=100

if [ "$brightness_path" = "" ]; then
    touch "brightness.tmp"
    echo "$brightness" > "brightness.tmp"
else
    brightness=$(cat $brightness_path)
fi

# TODO make this less resource intensive
if [ "$display" = "" ]; then
    display=$(xrandr -q | grep -w connected | cut -d" " -f1)
    export display
fi

path_pid="/tmp/polybar-brightness-simplistic"

if [ "$1" = "inc" ]; then
    if [ $brightness -lt 100 ]; then
        brightness=$(($brightness+$2))
    fi
elif [ "$1" = "dec" ]; then
    if [ $brightness -gt 0 ]; then
        brightness=$(($brightness-$2))
    fi
fi

echo "Brightness: ${brightness} %"

if [ "$1" = "inc" -o "$1" = "dec" ] ; then
    brightness_percent=$(echo "scale=2; $brightness/100"|bc)
    xrandr --output $display --brightness $brightness_percent
    echo "$brightness" > "brightness.tmp"
fi
