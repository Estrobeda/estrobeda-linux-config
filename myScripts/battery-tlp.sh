#bin/bash

check_battery() {
battery=$(sudo tlp-stat -b | grep "Charge" | tr -d -c ":digit:],.")
status=$((cut -d '=' -f 2 <<< $(sudo tlp-stat -b | grep status) | tr -d -c "[:alpha:],."))

mode="err"
if [  $status -eq "Charging" ]; then
		mode="AC"
else
		mode="DC"
fi

echo "# $mode $battery %"
}


while true; do
		check_battery

		sleep $1 &
		wait
done
