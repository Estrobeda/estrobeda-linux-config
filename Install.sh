#bin/sh
username=""

dependencies="fail"
i3Install="fail"
polybarInstall="fail"
i3themerInstall="fail"
zshInstall="fail"
comptonInstall="fail"
lightDmInstall="fail"
fzfInstall="fail"
vundleInstall="fail"
vimColabInstall="fail"
nodejsInstall="fail"
dotnetInstall="fail"
spotifyInstall="fail"

if [ ! -z "${SUDO_USER}" ]; then
    username="$SUDO_USER"
    echo "Do not run this script as sudo!!"
    exit
else
    username="$USER"
fi

tmp="/home/$username/.tmp_install/"
tmp_fonts="$tmp/fonts"
tmp_deps="$tmp/deps"
mkdir $tmp
mkdir "$tmp/fonts"
mkdir "$tmp/deps"

# Installation dependencies
sudo apt-get update -y
sudo apt-get install -y git curl

{
# Window manager
sudo apt install -y libyajl-dev \                               # i3-gaps Dependency
                            libstartup-notification0-dev \      # i3-gaps Dependency
                            libpango1.0-dev \                   # i3-gaps dependency
                            libev-dev \                         # i3-gaps Dependency
                            libxkbcommon-dev \                  # i3-gaps Dependency
                            libxkbcommon-x11-dev \              # i3-gaps Dependency
                            autoconf libxcb-xrm0 \              # i3-gaps Dependency
                            libxcb-xrm-dev \                    # i3-gaps Dependency
                            automake \                          # i3-gaps dependency
                            libxcb1-dev \                       # i3-gaps, Polybar dependency
                            libxcb-keysyms1-dev \               # i3-gaps, Polybar dependency
                            libxcb-util0-dev \                  # i3-gaps, Polybar dependency
                            libxcb-icccm4-dev \                 # i3-gaps, Polybar dependency                            
                            libxcb-randr0-dev \                 # i3-gaps, Polybar dependency
                            libxcb-cursor-dev \                 # i3-gaps, Polybar dependency
                            libxcb-xinerama0-dev \              # i3-gaps, Polybar dependency
                            libxcb-xkb-dev \                    # i3-gaps, Polybar dependency
                            libxcb-shape0-dev \                 # i3-gaps, Polybar dependency
                            xcb-proto \                         # i3-gaps, Polybar dependency
                            python \                            # i3-gaps, i3wm-themer, Polybar dependency
                            libxcb-image0-dev \                 # Polybar dependency
                            libxcb-ewmh-dev \                   # Polybar dependency
                            libxcb-icccm4 \                     # Polybar dependency  
                            libcairo-dev \                      # Polybar dependency
                            libxcb-xtest0-dev \                 # Polybar dependency
                            unifont \                           # Polybar dependency
                            libjsoncpp-dev \                    # Polybar dependency
                            python3 \                           # i3wm-themer dependency
                            python3-pip \                       # i3wm-themer dependency
                            fonts-font-awesome \                # i3wm-themer dependency
                            nitrogen \                          # i3wm-themer dependency
                            alsa-base \                         # i3wm-themer dependency
                            alsa-utils \                        # i3wm-themer dependency                        
                            mate-power-manager \                # i3wm-themer dependency
                            vim-gtk                             # Vim with clipboard for QT distros
    dependencies="succ"
} || {
    dependencies="fail"
    echo "Failed to install all the dependencies! Exiting..."
    exit
}

# Polybar build dependencies
#git clone https://github.com/stark/siji "${tmp_fonts}/siji"
#cd "${tmp_fonts}/siji"
#,/install.sh
# xset +fp /home/estrobeda/.local/share/fonts
# xset fp rehash

# ===== Install i3-gaps ===== 
{
git clone https://www.github.com/Airblader/i3 "${tmp_deps}/i3-gaps"
cd "${tmp_deps}/i3-gaps"

autoreconf --force --install
rm -rf build/
mkdir -p build && cd build/

../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers
make
make install
i3Install="succ"
} || {
 i3Install="fail"
 echo "Failed to install i3wm! Exiting..."
 exit
}

# make python3 default
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 10

# ===== Install Polybar =====
{
curl -s https://api.github.com/repos/polybar/polybar/releases/latest \
      | grep "browser_download_url" \
      | cut -d : -f 2,3 \
      | tr -d \" \
      | wget -qi -

# get the filename
archive=$(ls | grep polybar*.tar)
tar xvf "$archive" -C "/home/$username/${tmp_deps}"
wait 10
# Remove the archive
rm -rf $archive

cd "/home/$username/${tmp_deps}/polybar"
mkdir build
cd build
cmake ..
make -j$(nproc)
sudo make install
make userconfig
polybarInstall="succ"
} || {
    polybarInstall="fail"
    echo "Failed to install polybar! Exiting..."
    exit
}

# ===== Install i3-themer =====
{
git clone https://github.com/unix121/i3wm-themer /home/$username/.i3wm-themer
cd /home/$username/.i3wm-themer
pip3 install -r requirements.txt
./install_ubuntu.sh

sed -i "s/USER/$username/g" defaults/config.yaml

cp -r scripts/* /home/$username/.config/polybar/
mkdir /home/$username/Backups

python i3wm-themer.py --config config.yaml --backup /home/$username/Backups
python i3wm-themer.py --config config.yaml --install defaults/
python i3wm-themer.py --config config.yaml --load themes/004.json
i3themerInstall="succ"
} || {
    i3themerInstall="fail"
    echo "Failed to install i3wm-themer! Exiting..."
    exit
}

# ===== Install Compton =====
{
sudo apt-get install -y compton
comptonInstall="succ"
} || {
    comptonInstall="fail"
    echo "Failed to install compton composer! Continuing..."
}

# ===== Install ZSH =====
{
sudo apt-get install -y zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
zshInstall="succ"
} || {
    zshInstall="fail"
    echo "Failed to install zsh! Continuing..."
}

i
# ===== Install greeter =====
{
sudo apt-get install -y lightdm \
                        slick-greeter \
                        lightdm-settings
lightDmInstall="succ"
} || {
    lightDmInstall="fail"
    echo "Failed to install lightDm login panel! Continuing..."
}

if [[ "$lightDmInstall" == "succ" ]]; then
    sudo cp lightdm.conf /etc/lightdm/
    echo "configured lightdm"
fi


# ===== Install Utilities =====
{
git clone --depth 1 https://github.com/junegunn/fzf.git "/home/${username}.fzf"
~/.fzf/install
fzfInstall="succ"
} || {
    fzfInstall="fail"
    echo "Failed to install FZF utility! Continuing..."
}

{
git clone https://github.com/VundleVim/Vundle.vim.git "/home/${username}/.vim/bundle/Vundle.vim"
vundleInstall="succ"
} || {
    vundleInstall="fail"
    echo "Failed to install Vundle Utility! Continuing..."
}

{
pip install twisted argparse service_identity
vimColabInstall="succ"
} || {
    vimColabInstall="fail"
    echo "Failed to install Vim collaboration tools! Continuing..."
}

{
sudo apt-get install -y nodejs
curl -L https://npmjs.org/install.sh | sudo sh
nodejsInstall="succ"
} || {
    nodejsInstall="fail"
    echo "Failed to install NodeJS & NPM! Continuing..."
}

{
# dotnet
wget -q https://packages.microsoft.com/config/ubuntu/19.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
sudo apt-get update -y
sudo apt-get install -y apt-transport-https
sudo apt-get update -y
sudo apt-get install -y dotnet-sdk-3.1
dotnetInstall="succ"
} || {
    dotnetInstall="fail"
    echo "Failed to install DotNET SDK! Continuing..."
}

{
# spotify
curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-get update -y && sudo apt-get install -y spotify-client
spotifyInstall="succ"
} || {
    spotifyInstall="fail"
    echo "Failed to install Spotify! Continuing..."
}

echo "The dependencies were installed with status code:    [$dependencies]"
echo "i3 was installed with status code:                   [$i3Install]"
echo "Polybar was installed with status code:              [$polybarInstall]"
echo "i3wm-themer was installed with status code:          [$i3themerInstall]"
echo "Compton was installed with status code:              [$comptonInstall]"
echo "ZSH was installed with status code:                  [$zshInstall]"
echo "LightDM was installed with status code:              [$lightDmInstall]"
echo "FzF was installed with status code:                  [$fzfInstall]"
echo "VundleVim was installed with status code:            [$vundleInstall]"
echo "VundleCollab tools was installed with status code:   [$vimColabInstall]"
echo "NodeJS % NPM was installed with status code:         [$nodejsInstall]"
echo "DotNET SDK was installed with status code:           [$dotnetInstall]"
echo "Spotify was installed with status code:              [$spotifyInstall]"

echo "You'll have to reboot for all the changes to take effect
